const http=require('http');
const https=require('https');
const server=http.createServer((req,res)=>{
    req.headers.host="electron.textnow.com";
    var rq=https.request(
        `https://electron.textnow.com${req.url}`,
        {
            method: req.method,
            headers: req.headers
        },
        rs => {
            res.writeHead(rs.statusCode,rs.headers)
            rs.on('data', chunk => {
                res.write(chunk)
            })
            rs.on('end', _ => {
                res.end();
            })
        }
    );
    req.on('data',chunk => {
        rq.write(chunk)
    })
    req.on('end', _ => {
        rq.end()
    })
})
server.listen(443)